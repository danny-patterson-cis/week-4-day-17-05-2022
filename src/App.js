import React, { Component } from 'react'
import './App.css'
import Form from './Form'
import LifecycleA from './components/LifecycleA'
import FragmentDemo from './components/FragmentDemo'
import Table from './components/Table'
import PureComp from './components/PureComp'
import RegComp from './components/RegComp'
import ParentComp from './components/ParentComp'
import RefsDemo from './components/RefsDemo'
import FocusInput from './components/FocusInput'
import FRParentInput from './components/FRParentInput'
import FRInput from './components/FRInput'
// import PortalDemo from './components/PortalDemo'
import ClickCounter from './components/ClickCounter'
import HoverCounter from './components/HoverCounter'
class App extends Component {
	render() {
		return (
			<div className="App">
				<Form />
        <LifecycleA />
        <FragmentDemo/>
        <Table/>
        <PureComp/>
        <RegComp/>
        <ParentComp/>
        <RefsDemo/>
        <FocusInput/>
        <FRParentInput/>
        <FRInput/>
        {/* <PortalDemo/> */}
        <ClickCounter/>
        <HoverCounter/>
        
			</div>
		)
	}
}

export default App
